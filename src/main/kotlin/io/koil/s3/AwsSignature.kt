package io.koil.s3

import io.vertx.core.Future
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientRequest
import io.vertx.core.http.HttpMethod
import java.security.MessageDigest
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*
import javax.crypto.Mac
import javax.crypto.SecretKey
import javax.crypto.spec.SecretKeySpec

private const val HMAC_ALG = "HmacSHA256"
private const val HASH_ALG = "SHA-256"
private val multipleSpacesRegex = Regex(" +")

class SignatureRequest(
    private val method: HttpMethod,
    private val uri: String,
) {
    // We want a tree map for values to be sorted automagically
    private val headers = TreeMap<String, MutableList<String>>()
    private val noContentHash = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
    private val dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd'T'HHmmss'Z'")
        .withZone(ZoneOffset.UTC)

    private fun normalizeValue(value: String): String {
        return value.split("\n")
            .asSequence()
            .map {
                it.trim().replace(multipleSpacesRegex, " ")
            }
            .joinToString(",")
    }

    fun addHeader(name: String, value: String): SignatureRequest = apply {
        val lowercaseName = name.lowercase()
        val normalizedValue = normalizeValue(value)

        headers.computeIfAbsent(lowercaseName) { mutableListOf() }
            .add(normalizedValue)
    }

    private fun computeHeaderNames(): String = headers.keys.joinToString(";")
    private val canonicalHeaders: String
        get() {
            val sb = StringBuilder()
            headers.forEach { (k, v) ->
                sb.append(k)
                    .append(':')
                    .append(v.joinToString(","))
                    .append('\n')
            }
            return sb.toString()
        }

    private fun createCanonicalRequest(headerNames: String, payloadHash: String): String {
        return "$method\n$uri\n\n$canonicalHeaders\n$headerNames\n$payloadHash"
    }

    fun create(signer: AwsSignature, client: HttpClient, payloadHash: String? = null): Future<HttpClientRequest> {
        val formattedDateTime = dateTimeFormatter.format(Instant.now())
        addHeader("X-Amz-Content-Sha256", payloadHash ?: noContentHash)
        addHeader("X-Amz-Date", formattedDateTime)

        val headerNames = computeHeaderNames()
        val authHeader = signer.computeSignature(
            createCanonicalRequest(headerNames, payloadHash ?: noContentHash),
            headerNames,
            formattedDateTime,
        )

        return client.request(method, uri)
            .map {
                headers.forEach(it::putHeader)
                it.putHeader("Authorization", authHeader)
            }
    }
}

/**
 * AWS Signature v4
 */
class AwsSignature(
    private val accessKey: String,
    private val secretKey: String,
    private val region: String,
    private val service: String,
) {
    private val mac: Mac = Mac.getInstance(HMAC_ALG)
    private val md = MessageDigest.getInstance(HASH_ALG)
    private val hexFormat = HexFormat.of()

    private var signingKeyDate = LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE)
    private var signingKey = computeSigningKey(signingKeyDate)

    private fun computeSigningKey(date: String): SecretKey {
        mac.init(SecretKeySpec("AWS4$secretKey".toByteArray(Charsets.UTF_8), HMAC_ALG))
        val dateKey = mac.doFinal(date.toByteArray(Charsets.UTF_8))

        mac.init(SecretKeySpec(dateKey, HMAC_ALG))
        val dateRegionKey = mac.doFinal(region.toByteArray(Charsets.UTF_8))

        mac.init(SecretKeySpec(dateRegionKey, HMAC_ALG))
        val dateRegionServiceKey = mac.doFinal(service.toByteArray(Charsets.UTF_8))

        mac.init(SecretKeySpec(dateRegionServiceKey, HMAC_ALG))
        return SecretKeySpec(mac.doFinal("aws4_request".toByteArray(Charsets.UTF_8)), HMAC_ALG)
    }

    private val staticCredentialScopePart = "/$region/$service/aws4_request"
    private fun computeCredentialScope(formattedDate: String): String = "$formattedDate$staticCredentialScopePart"

    fun computeSignature(canonicalRequest: String, headerNames: String, formattedDateTime: String): String {
        val formattedDate = LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE)
        val credentialScope = computeCredentialScope(formattedDate)

        // Refresh the signing key if needed
        if(signingKeyDate != formattedDate) {
            signingKeyDate = formattedDate
            signingKey = computeSigningKey(formattedDate)
        }

        val hashedCanonicalRequest = hexFormat.formatHex(md.digest(canonicalRequest.toByteArray(Charsets.UTF_8)))
        val toSign = "AWS4-HMAC-SHA256\n$formattedDateTime\n$credentialScope\n$hashedCanonicalRequest"

        mac.init(signingKey)
        val signature = hexFormat.formatHex(mac.doFinal(toSign.toByteArray(Charsets.UTF_8)))

        return "AWS4-HMAC-SHA256 Credential=$accessKey/$credentialScope,SignedHeaders=$headerNames,Signature=$signature"
    }

    fun hashContent(content: Buffer): String {
        return hexFormat.formatHex(md.digest(content.bytes))
    }
}
