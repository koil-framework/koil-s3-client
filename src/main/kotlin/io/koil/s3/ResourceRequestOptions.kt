package io.koil.s3

import kotlin.math.max
import kotlin.math.min

class ResourceRequestOptions {
    var byteRange: Pair<Long, Long>? = null
        private set

    fun byteRange(start: Long, end: Long): ResourceRequestOptions =
        apply { byteRange = Pair(min(start, end), max(start, end)) }
}
