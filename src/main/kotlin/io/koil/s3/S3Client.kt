package io.koil.s3

import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpClient
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpClientResponse
import io.vertx.core.http.HttpMethod
import io.vertx.core.impl.NoStackTraceException
import io.vertx.core.net.PemKeyCertOptions
import io.vertx.core.net.PemTrustOptions
import io.vertx.core.streams.ReadStream
import org.slf4j.LoggerFactory
import org.slf4j.MarkerFactory

class S3ClientError(val statusCode: Int, val responseBody: Buffer) :
    NoStackTraceException("Got status code $statusCode")

// TODO Ability to reload config on the fly

class S3Client(vertx: Vertx, private val options: S3ClientOptions) {
    companion object {
        private val logger = LoggerFactory.getLogger(S3Client::class.java)
    }

    private val client: HttpClient
    private val signer = AwsSignature(
        options.accessKey,
        options.secretKey,
        options.region,
        "s3"
    )

    init {
        val opts = HttpClientOptions()
            .setDefaultHost(options.host)
            .setDefaultPort(options.port)
            .setSsl(options.ssl)
            .setKeepAlive(true)

        if (options.trustAll) {
            opts.setTrustAll(true)
        }

        if (!options.caCertPath.isNullOrBlank()) {
            opts.setTrustOptions(
                PemTrustOptions()
                    .addCertPath(options.caCertPath)
            )
        }

        if (!options.certPath.isNullOrBlank() && !options.keyPath.isNullOrBlank()) {
            opts.setKeyCertOptions(
                PemKeyCertOptions()
                    .addCertPath(options.certPath)
                    .addKeyPath(options.keyPath)
            )
        }

        client = vertx.createHttpClient(opts)
    }

    private fun executeGet(uri: String, opts: ResourceRequestOptions): Future<HttpClientResponse> {
        val req = SignatureRequest(HttpMethod.GET, uri)
            .addHeader("Host", options.host)

        if (opts.byteRange != null) {
            val (start, end) = opts.byteRange!!
            req.addHeader("Range", "bytes=$start-$end")
        }

        return req.create(signer, client)
            .compose {
                it.putHeader("Accept-Encoding", "gzip")
                    .putHeader("User-Agent", options.userAgent)
                    .send()
            }
    }

    fun getResourceAsStream(
        bucket: String,
        filename: String,
        opts: ResourceRequestOptions = ResourceRequestOptions()
    ): Future<S3StreamGetResponse> {
        val uri = "/$bucket/$filename"
        return executeGet(uri, opts)
            .compose { res ->
                handleResponse(uri, res, if (opts.byteRange != null) 206 else 200) {
                    it.pause()
                    Future.succeededFuture(
                        S3StreamGetResponse(
                            it.headers(),
                            it.statusCode(),
                            it.getHeader("ETag"),
                            it.getHeader("Content-Type"),
                            it.getHeader("X-Amz-Request-Id"),
                            it,
                        )
                    )
                }
            }
    }

    fun getResource(
        bucket: String,
        filename: String,
        opts: ResourceRequestOptions = ResourceRequestOptions()
    ): Future<S3GetResponse> {
        val uri = "/$bucket/$filename"
        return executeGet(uri, opts)
            .compose { res ->
                handleResponse(uri, res, if (opts.byteRange != null) 206 else 200) {
                    it.body().map { body ->
                        S3GetResponse(
                            it.headers(),
                            it.statusCode(),
                            it.getHeader("ETag"),
                            it.getHeader("Content-Type"),
                            it.getHeader("X-Amz-Request-Id"),
                            body,
                        )
                    }
                }
            }
    }

    /**
     * Creates a resource
     *
     * @param bucket the name of the bucket to put the resource into
     * @param filename the name of the resource
     * @param contentType the content type of the resource
     * @param contentHash the SHA256 hash of the content, hex encoded
     * @param content the actual content
     */
    fun putResource(
        bucket: String,
        filename: String,
        contentType: String,
        contentHash: String,
        content: Buffer
    ): Future<Unit> {
        val uri = "/$bucket/$filename"
        return SignatureRequest(HttpMethod.PUT, uri)
            .addHeader("Host", options.host)
            .addHeader("Content-Type", contentType)
            .create(signer, client, contentHash)
            .compose {
                it.putHeader("Accept-Encoding", "gzip")
                    .putHeader("User-Agent", options.userAgent)
                    .send(content)
            }
            .compose { handleResponse(uri, it, 200) { Future.succeededFuture(Unit) } }
    }

    /**
     * Creates a resource, hashing it in the process
     *
     * @param bucket the name of the bucket to put the resource into
     * @param filename the name of the resource
     * @param contentType the content type of the resource
     * @param content the actual content
     */
    fun putResource(
        bucket: String,
        filename: String,
        contentType: String,
        content: Buffer
    ): Future<Unit> {
        val uri = "/$bucket/$filename"
        return SignatureRequest(HttpMethod.PUT, uri)
            .addHeader("Host", options.host)
            .addHeader("Content-Type", contentType)
            .create(signer, client, signer.hashContent(content))
            .compose {
                it.putHeader("Accept-Encoding", "gzip")
                    .putHeader("User-Agent", options.userAgent)
                    .send(content)
            }
            .compose { handleResponse(uri, it, 200) { Future.succeededFuture(Unit) } }
    }

    /**
     * Creates a resource, hashing it in the process
     *
     * @param bucket the name of the bucket to put the resource into
     * @param filename the name of the resource
     * @param contentType the content type of the resource
     * @param contentHash the SHA256 hash of the content, hex encoded
     * @param content the actual content
     */
    fun putResourceAsStream(
        bucket: String,
        filename: String,
        contentType: String,
        contentLength: Long,
        contentHash: String,
        content: ReadStream<Buffer>,
    ): Future<Unit> {
        val uri = "/$bucket/$filename"
        return SignatureRequest(HttpMethod.PUT, uri)
            .addHeader("Host", options.host)
            .addHeader("Content-Type", contentType)
            .create(signer, client, contentHash)
            .compose {
                it.putHeader("User-Agent", options.userAgent)
                    .putHeader("Content-Length", contentLength.toString())
                    .send(content.resume())
            }
            .compose { handleResponse(uri, it, 200) { Future.succeededFuture(Unit) } }
    }

    fun deleteResource(
        bucket: String,
        filename: String,
    ): Future<Unit> {
        val uri = "/$bucket/$filename"
        return SignatureRequest(HttpMethod.DELETE, uri)
            .addHeader("Host", options.host)
            .create(signer, client)
            .compose { it.putHeader("User-Agent", options.userAgent).send() }
            .compose { handleResponse(uri, it, 204) { Future.succeededFuture(Unit) } }
    }

    private fun <T> handleResponse(
        uri: String,
        res: HttpClientResponse,
        expectedStatusCode: Int,
        handler: (HttpClientResponse) -> Future<T>,
    ): Future<T> {
        val loggingEvent = logger.atTrace()
            .addKeyValue("method", "DELETE")
            .addKeyValue("path", uri)
            .addKeyValue("statusCode", res.statusCode())

        return if (res.statusCode() != expectedStatusCode) {
            loggingEvent.addMarker(MarkerFactory.getMarker("s3_error"))
            res.body().compose {
                loggingEvent.log("Failed: {}", it.toString(Charsets.UTF_8))
                Future.failedFuture(S3ClientError(res.statusCode(), it))
            }
        } else {
            loggingEvent.log("Success")
            handler(res)
        }
    }
}
