package io.koil.s3

class S3ClientOptions(
    val accessKey: String,
    val secretKey: String,
) {
    var ssl = true
        private set
    var port = 443
        private set
    var region = "us-east-1"
        private set
    var host = "s3.us-east-1.amazonaws.com"
        private set
    var userAgent = "Koil/1.0 (unknown) VertxHttpClient/4.5.5"
        private set
    var trustAll = false
        private set
    var caCertPath: String? = null
        private set

    /** The client certificate path for mTLS */
    var certPath: String? = null
        private set

    /** The client private key path for mTLS */
    var keyPath: String? = null
        private set

    fun userAgent(userAgent: String): S3ClientOptions = apply { this.userAgent = userAgent }
    fun port(port: Int): S3ClientOptions = apply { this.port = port }
    fun host(host: String): S3ClientOptions = apply { this.host = host }
    fun region(region: String): S3ClientOptions = apply {
        this.region = region
        if (host == "s3.us-east-1.amazonaws.com") {
            host = "s3.$region.amazonaws.com"
        }
    }

    fun ssl(ssl: Boolean): S3ClientOptions = apply {
        if (!ssl && this.ssl && port == 443) port = 80
        else if (ssl && !this.ssl && port == 80) port = 443
        this.ssl = ssl
    }

    fun trustAll(trustAll: Boolean = true): S3ClientOptions = apply { this.trustAll = trustAll }

    fun caCertPath(caCertPath: String): S3ClientOptions = apply { this.caCertPath = caCertPath }

    /** The client certificate path for mTLS */
    fun certPath(certPath: String): S3ClientOptions = apply { this.certPath = certPath }

    /** The client private key path for mTLS */
    fun keyPath(keyPath: String): S3ClientOptions = apply { this.keyPath = keyPath }
}
