package io.koil.s3

import io.vertx.core.MultiMap
import io.vertx.core.buffer.Buffer
import io.vertx.core.streams.ReadStream

data class S3GetResponse(
    val headers: MultiMap,
    val statusCode: Int,
    val etag: String,
    val contentType: String,
    val requestId: String?,
    val body: Buffer,
)

data class S3StreamGetResponse(
    val headers: MultiMap,
    val statusCode: Int,
    val etag: String,
    val contentType: String,
    val requestId: String?,
    val stream: ReadStream<Buffer>,
)
